import OldHeader from "@/components/OldHeader";

export default function Home() {
  return (
    <>
      <div className="bg-gray-100 w-auto h-96"></div>
      <OldHeader />
    </>
  );
}
