import React from "react";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "./ui/dropdown-menu";
import Image from "next/image";
import Link from "next/link";

interface HeaderLinkButtonProps {
  text: string;
  url: string;
}

function HeaderLinkButton({ text, url }: HeaderLinkButtonProps) {
  return (
    <>
      <div className="flex justify-center items-center">
        <Link className="cursor-pointer pr-2" href={url}>
          {text}
        </Link>
      </div>
    </>
  );
}

export default HeaderLinkButton;
