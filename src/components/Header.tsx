import React from "react";
import Image from "next/image";
import Link from "next/link";
import NavMenu from "./NavMenu";

function Header() {
  return (
    <>
      <div className="items-center inline-flex py-5 w-auto select-none">
        <Link href="/">
          <Image
            src="/bps_logo.png"
            alt="BPS Logo"
            width={369}
            height={128}
            className="pr-28 scale-75 origin-left"
            draggable={false}
          />
        </Link>

        <div className="font-bold flex gap-10">
          <NavMenu />
        </div>
      </div>
    </>
  );
}

export default Header;
