import React from "react";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "./ui/dropdown-menu";

import Image from "next/image";
import Link from "next/link";

interface HeaderDropdownButtonProps {
  text: string;
  dropdownItems: string[];
  dropdownLinks: string[];
}

function HeaderDropdownButton({
  text,
  dropdownItems,
  dropdownLinks,
}: HeaderDropdownButtonProps) {
  return (
    <>
      <div className="flex justify-center items-center">
        <DropdownMenu>
          <DropdownMenuTrigger className="select-none">
            <span className="cursor-pointer pr-2">{text}</span>
            <Image
              src="/dropdown.png"
              alt="Dropdown"
              height={20}
              width={20}
              className="inline"
            ></Image>
          </DropdownMenuTrigger>
          <DropdownMenuContent>
            {dropdownItems.map((item, index) => (
              <Link href={dropdownLinks[index]} key={index}>
                <DropdownMenuItem
                  key={index}
                  className="flex justify-center font-bold"
                >
                  {item}
                </DropdownMenuItem>
              </Link>
            ))}
          </DropdownMenuContent>
        </DropdownMenu>
      </div>
    </>
  );
}

export default HeaderDropdownButton;
