import React from "react";
import HeaderDropdownButton from "./HeaderDropdownButton";
import HeaderLinkButton from "./HeaderLinkButton";

function OldHeader() {
  return (
    <>
      <div>
        <HeaderDropdownButton
          text="Korcsoportok"
          dropdownItems={["3-6", "6-12", "12-14", "14-18"]}
          dropdownLinks={[
            "/korcsoportok/3-6",
            "/korcsoportok/6-12",
            "/korcsoportok/12-14",
            "/korcsoportok/14-18",
          ]}
        />
        <HeaderDropdownButton
          text="Bemutatkozás"
          dropdownItems={["1", "2", "3", "4"]}
          dropdownLinks={[
            "/bemutatkozasok/1",
            "/bemutatkozasok/2",
            "/bemutatkozasok/3",
            "/bemutatkozasok/4",
          ]}
        />
        <HeaderLinkButton url="/" text="BPS LAB" />
      </div>
    </>
  );
}

export default OldHeader;
